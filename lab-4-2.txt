####################
## First Pipeline ##
####################

# Gitlab
1. Open gitlab (https://gitlab.com/) and create new project
Project name : jenkins_practice

2. Create Jenkinsfile
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'You are in Build Stage :)'
            }
        }
        stage('Test') {
            steps {
                echo 'You are in Testing Stage :)'
            }
        }
        stage('Deploy') {
            steps {
                echo 'You are in Deploying Stage :)'
            }
        }
    }
}

3. Open Blue Ocean in Jenkins and create New Pipeline

4. Where do you store your code? Git

5. Open jenkins_practice project in gitlab and copy ssh address

6. Open Blue Ocean and paste the ssh address in Repository URL

7. Copy public ssh key to gitlab
Edit profile - SSH Keys - Add key

8. Back to Blue Ocean and Create Pipeline

# Github
1. Open github and create new repository
Repository name : jenkins_practice

2. Create Jenkinsfile
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'You are in Build Stage Github :)'
            }
        }
        stage('Test') {
            steps {
                echo 'You are in Testing Stage Github :)'
            }
        }
        stage('Deploy') {
            steps {
                echo 'You are in Deploying Stage Github :)'
            }
        }
    }
}

3. Open Blue Ocean in Jenkins and create New Pipeline

4. Where do you store your code? GitHub

5. Create access token in github
Settings - Developer settings - Personal access tokens - Generate new token

Note : Jenkins Practice
Select scopes : repo & user

6. Copy token to Blue Ocean

7. Choose a repository and Create Pipeline